
interface UserPasswordRepPayload
{
    getPassword(): string;
    getPasswordConfirmation(): string;
}

export default UserPasswordRepPayload;
