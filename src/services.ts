
export enum SERVICES {
    IAuthService = 'IAuthService',
    IFileService = 'IFileService',
    IRoleService = 'IRoleService',
    IUserService = 'IUserService',
    IItemService = 'IItemService',
    INotificationService = 'INotificationService'
}
