import { NextFunction, Request, Response } from 'express';
import { inject } from 'inversify';
import { controller, httpPost, next, request, response } from 'inversify-express-utils';
import { StatusCode } from '@digichanges/shared-experience';

import { TYPES } from '../../../../types';
import Responder from '../../../../App/Presentation/Shared/Express/Responder';

import ExerciseController from '../../Controllers/ExerciseController';
import CirclesRepRequest from '../../Requests/CirclesRepRequest';

@controller('/api/exercises')
class ExerciseHandler
{
    @inject(TYPES.Responder)
    private responder: Responder;
    private readonly controller: ExerciseController;

    constructor()
    {
        this.controller = new ExerciseController();
    }

    @httpPost('/circles')
    public async circles(@request() req: Request, @response() res: Response, @next() nex: NextFunction)
    {
        const _request = new CirclesRepRequest(req.body);

        const data = await this.controller.circles(_request);

        this.responder.send(data, req, res, StatusCode.HTTP_OK);
    }

}

export default ExerciseHandler;
