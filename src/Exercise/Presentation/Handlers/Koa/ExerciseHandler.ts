import Koa from 'koa';
import Router from 'koa-router';
import { StatusCode } from '@digichanges/shared-experience';
import Responder from '../../../../App/Presentation/Shared/Koa/Responder';
import ExerciseController from '../../Controllers/ExerciseController';
import CirclesRepRequest from '../../Requests/CirclesRepRequest';

const routerOpts: Router.IRouterOptions = {
    prefix: '/api/exercises'
};

const ExerciseHandler: Router = new Router(routerOpts);
const responder: Responder = new Responder();
const controller: ExerciseController = new ExerciseController();


ExerciseHandler.post('/circles', async(ctx: Koa.ParameterizedContext & any) =>
{
    const request = new CirclesRepRequest(ctx.request.body);

    const data = await controller.circles(request);

    responder.send(data, ctx, StatusCode.HTTP_OK);
});


export default ExerciseHandler;
