import ValidatorRequest from '../../../App/Presentation/Shared/ValidatorRequest';
import CirclesRepPayload from '../../InterfaceAdapters/Payloads/CirclesRepPayload';
import CircleUseCase from '../../Domain/UseCases/CircleUseCase';

class ExerciseController
{
  
    public async circles(request: CirclesRepPayload)
    {
        await ValidatorRequest.handle(request);

        const useCase = new CircleUseCase();
        return await useCase.handle(request);
    }
}

export default ExerciseController;
