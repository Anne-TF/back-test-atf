import { ArrayMinSize, IsArray, ValidateNested } from 'class-validator';
import { Type } from 'class-transformer';
import CirclesRepPayload from '../../InterfaceAdapters/Payloads/CirclesRepPayload';
import CirclePayload from '../../InterfaceAdapters/Payloads/CirclePayload';
import CircleRequest from './CircleRequest';

class CirclesRepRequest implements CirclesRepPayload
{
    @IsArray()
    @ArrayMinSize(2)
    @ValidateNested({ each: true })
    @Type(() => CircleRequest)
    protected circles: CirclePayload[];

    constructor(data: Record<string, any>)
    {
        this.circles = data?.circles?.map((circle: Record<string, any>) => new CircleRequest(circle));
    }

    getCircles(): CirclePayload[]
    {
        return this.circles;
    }
}

export default CirclesRepRequest;
