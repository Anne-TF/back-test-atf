import { IsNumber, IsString } from 'class-validator';
import CirclePayload from '../../InterfaceAdapters/Payloads/CirclePayload';

class CircleRequest implements CirclePayload
{
    @IsString()
    protected name: string;

    @IsNumber()
    protected x: number;

    @IsNumber()
    protected y: number;

    @IsNumber()
    protected radius: number;

    constructor(data: Record<string, any>)
    {
        this.name = data.name;
        this.x = data.x;
        this.y = data.y;
        this.radius = data.radius;
    }

    getName(): string
    {
        return this.name;
    }

    getX(): number
    {
        return this.x;
    }

    getY(): number
    {
        return this.y;
    }

    getRadius(): number
    {
        return this.radius;
    }
}

export default CircleRequest;
