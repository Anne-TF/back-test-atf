import CirclePayload from '../../InterfaceAdapters/Payloads/CirclePayload';
import CirclesRepPayload from '../../InterfaceAdapters/Payloads/CirclesRepPayload';

interface CircleResult
{
    [key: string]: any;
    touch: string[];
    noTouch: string[];
    intercepted: string[];
}

class CircleUseCase
{
    async handle(payload: CirclesRepPayload): Promise<any>
    {
        return { result: this.findCircleCollisions(payload.getCircles()) };
    }

    private findCircleCollisions(circles: CirclePayload[]): CircleResult[]
    {

        const out: CircleResult[] = [];

        circles.forEach((circle, i) =>
        {
            const result: CircleResult = {
                ...circle,
                touch: [],
                noTouch: [],
                intercepted:[]
            };

            circles.forEach((_circle, _i) =>
            {
                if (_i !== i)
                {
                    const distSq = Math.pow((circle.getX() - _circle.getX()), 2) + Math.pow((circle.getY() - _circle.getY()), 2);
                    const radSumSq = Math.pow((circle.getRadius() + _circle.getRadius()), 2);
                    if (distSq === radSumSq)
                    {
                        result.touch.push(_circle.getName());
                    }
                    else if (distSq > radSumSq)
                    {
                        result.noTouch.push(_circle.getName());
                    }
                    else if (distSq < radSumSq)
                    {
                        result.intercepted.push(_circle.getName());
                    }
                }
            });

            out.push(result);

        });

        return out;
    }
}

export default CircleUseCase;