interface CirclePayload
{
    getName(): string;
    getRadius(): number;
    getX(): number;
    getY(): number;
}

export default CirclePayload;