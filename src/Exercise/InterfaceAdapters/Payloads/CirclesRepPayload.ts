import CirclePayload from './CirclePayload';

interface CirclesRepPayload
{
    getCircles(): CirclePayload[];
}

export default CirclesRepPayload;